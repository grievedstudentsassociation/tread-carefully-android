package pl.gsa.treadcarefully

import android.app.Application
import pl.gsa.treadcarefully.injection.AppComponent
import pl.gsa.treadcarefully.injection.DaggerAppComponent
import pl.gsa.treadcarefully.injection.modules.AndroidModule

class App : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent.builder()
                .androidModule(AndroidModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }

}