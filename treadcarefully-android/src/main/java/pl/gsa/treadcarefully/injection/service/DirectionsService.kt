package pl.gsa.treadcarefully.injection.service

import android.util.Log
import com.google.android.gms.maps.model.LatLng
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.web.client.RestTemplate
import pl.gsa.treadcarefully.BuildConfig
import pl.gsa.treadcarefully.ErrorConsumer
import pl.gsa.treadcarefully.ResultConsumer
import pl.gsa.treadcarefully.dto.DirectionsRequestDTO
import pl.gsa.treadcarefully.dto.DirectionsResponseDTO
import pl.gsa.treadcarefully.dto.LatLngDTO
import pl.gsa.treadcarefully.utils.ConsumableAsyncTask
import javax.inject.Inject

class DirectionsService @Inject constructor(private val restTemplate: RestTemplate) : RestService() {

    fun findRoute(origin: LatLng, destination: LatLng,
                  resultConsumer: ResultConsumer<DirectionsResponseDTO>,
                  errorConsumer: ErrorConsumer<Throwable>) {
        FindRouteTask(origin, destination, resultConsumer, errorConsumer, restTemplate).execute()
    }

    companion object {

        private val TAG = DirectionsService::class.simpleName

        private class FindRouteTask
        constructor(val origin: LatLng, val destination: LatLng,
                    resultConsumer: ResultConsumer<DirectionsResponseDTO>,
                    errorConsumer: ErrorConsumer<Throwable>,
                    val restTemplate: RestTemplate)
            : ConsumableAsyncTask<Void, Void, DirectionsResponseDTO>(resultConsumer, errorConsumer) {

            companion object {
                private val TAG = FindRouteTask::class.simpleName
            }

            override fun backgroundTask(vararg params: Void): DirectionsResponseDTO? {
                Log.d(TAG, "Finding route from $origin to $destination")

                val request = DirectionsRequestDTO(
                        LatLngDTO(origin.latitude, origin.longitude),
                        LatLngDTO(destination.latitude, destination.longitude)
                )

                val result = restTemplate.exchange(
                        "${BuildConfig.REST_API_BASE_URL}/api/android/directions",
                        HttpMethod.POST,
                        HttpEntity(request),
                        DirectionsResponseDTO::class.java
                ).parsedBody

                Log.d(TAG, "Found ${result.points.size}-point route from $origin to $destination")
                return result
            }
        }

    }

}