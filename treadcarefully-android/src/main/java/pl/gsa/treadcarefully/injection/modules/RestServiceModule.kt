package pl.gsa.treadcarefully.injection.modules

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import dagger.Module
import dagger.Provides
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import javax.inject.Singleton

@Module
class RestServiceModule {

    @Provides
    @Singleton
    fun provideRestTemplate(): RestTemplate {
        val converter = MappingJackson2HttpMessageConverter()
        converter.objectMapper = ObjectMapper().registerKotlinModule()

        return RestTemplate(listOf(converter))
    }

}