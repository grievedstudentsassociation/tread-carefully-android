package pl.gsa.treadcarefully.injection.modules

import dagger.Module
import dagger.Provides
import org.springframework.web.client.RestTemplate
import pl.gsa.treadcarefully.injection.service.AreaService
import pl.gsa.treadcarefully.injection.service.DirectionsService
import javax.inject.Singleton

@Module(includes = [RestServiceModule::class])
class ServiceModule {

    @Provides
    @Singleton
    fun provideAreaService(restTemplate: RestTemplate) = AreaService(restTemplate)

    @Provides
    @Singleton
    fun provideDirectionsService(restTemplate: RestTemplate) = DirectionsService(restTemplate)

}