package pl.gsa.treadcarefully.injection.service

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpClientErrorException

abstract class RestService {

    companion object {

        val <T> ResponseEntity<T>.parsedBody: T get() = parseResponse(this)

        protected fun <T> parseResponse(responseEntity: ResponseEntity<T>): T {
            if (responseEntity.statusCode == HttpStatus.OK) {
                return responseEntity.body
            }
            throw HttpClientErrorException(responseEntity.statusCode)
        }

    }

}