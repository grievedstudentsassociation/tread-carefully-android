package pl.gsa.treadcarefully.injection.service

import android.util.Log
import com.google.android.gms.maps.model.LatLngBounds
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.web.client.RestTemplate
import pl.gsa.treadcarefully.BuildConfig
import pl.gsa.treadcarefully.ErrorConsumer
import pl.gsa.treadcarefully.ResultConsumer
import pl.gsa.treadcarefully.dto.AreaDTO
import pl.gsa.treadcarefully.dto.LatLngBoundsDTO
import pl.gsa.treadcarefully.dto.LatLngDTO
import pl.gsa.treadcarefully.utils.ConsumableAsyncTask
import javax.inject.Inject

class AreaService @Inject constructor(private val restTemplate: RestTemplate) : RestService() {

    fun getAllAreas(resultConsumer: ResultConsumer<List<AreaDTO>>,
                    errorConsumer: ErrorConsumer<Throwable>) {
        GetAllAreasTask(resultConsumer, errorConsumer, restTemplate).execute()
    }

    fun getAreasInBounds(latLngBounds: LatLngBounds,
                         resultConsumer: ResultConsumer<List<AreaDTO>>,
                         errorConsumer: ErrorConsumer<Throwable>) {
        GetAreasInBoundsTask(latLngBounds, resultConsumer, errorConsumer, restTemplate).execute()
    }

    companion object {

        private val TAG = AreaService::class.simpleName

        private class GetAllAreasTask
        constructor(resultConsumer: ResultConsumer<List<AreaDTO>>,
                    errorConsumer: ErrorConsumer<Throwable>,
                    val restTemplate: RestTemplate)
            : ConsumableAsyncTask<Void, Void, List<AreaDTO>>(resultConsumer, errorConsumer) {

            companion object {
                private val TAG = GetAllAreasTask::class.simpleName
            }

            override fun backgroundTask(vararg params: Void): List<AreaDTO>? {
                Log.d(TAG, "Getting all areas")

                val result = restTemplate.exchange(
                        "${BuildConfig.REST_API_BASE_URL}/api/area",
                        HttpMethod.GET,
                        null,
                        object : ParameterizedTypeReference<List<AreaDTO>>() {}
                ).parsedBody

                Log.d(TAG, "Fetched all areas: $result")
                return result
            }

        }

        private class GetAreasInBoundsTask
        constructor(val latLngBounds: LatLngBounds,
                    resultConsumer: ResultConsumer<List<AreaDTO>>,
                    errorConsumer: ErrorConsumer<Throwable>,
                    val restTemplate: RestTemplate)
            : ConsumableAsyncTask<Void, Void, List<AreaDTO>>(resultConsumer, errorConsumer) {

            companion object {
                private val TAG = GetAreasInBoundsTask::class.simpleName
            }

            override fun backgroundTask(vararg params: Void): List<AreaDTO>? {

                val boundsDTO = LatLngBoundsDTO(
                        LatLngDTO(latLngBounds.northeast.latitude,
                                latLngBounds.northeast.longitude),
                        LatLngDTO(latLngBounds.southwest.latitude,
                                latLngBounds.southwest.longitude))

                Log.d(TAG, "Getting areas for bounds: $boundsDTO")

                val result = restTemplate.exchange(
                        "${BuildConfig.REST_API_BASE_URL}/api/android/area",
                        HttpMethod.POST,
                        HttpEntity(boundsDTO),
                        object : ParameterizedTypeReference<List<AreaDTO>>() {}
                ).parsedBody

                Log.d(TAG, "Fetched areas for bounds: $result")
                return result
            }

        }

    }

}