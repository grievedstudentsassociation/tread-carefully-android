package pl.gsa.treadcarefully.injection

import dagger.Component
import pl.gsa.treadcarefully.App
import pl.gsa.treadcarefully.MainActivity
import pl.gsa.treadcarefully.injection.modules.AndroidModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidModule::class)])
interface AppComponent {

    fun inject(app: App)
    fun inject(mainActivity: MainActivity)

}