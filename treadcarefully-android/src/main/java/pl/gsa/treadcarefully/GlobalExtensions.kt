package pl.gsa.treadcarefully

import android.app.Activity

/**
 * Extension property for fetching actual App object without casting on every access
 */
val Activity.app: App get() = application as App

/**
 * Convenience aliases for defining consumer interfaces
 */
typealias ResultConsumer<T> = (T?) -> Unit
typealias ErrorConsumer<T> = (T) -> Unit