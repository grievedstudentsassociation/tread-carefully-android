package pl.gsa.treadcarefully

import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.TypedValue
import android.view.View
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.area_details_sheet.*
import kotlinx.android.synthetic.main.toolbar_main.*
import pl.gsa.treadcarefully.dto.AreaDTO
import pl.gsa.treadcarefully.dto.DirectionsResponseDTO
import pl.gsa.treadcarefully.enums.DangerLevel
import pl.gsa.treadcarefully.injection.service.AreaService
import pl.gsa.treadcarefully.injection.service.DirectionsService
import javax.inject.Inject

class MainActivity : AppCompatActivity(),
        OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnPolygonClickListener {

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        private const val KEY_CAMERA_POSITION = "camera_position"
        private const val KEY_LOCATION = "location"

        private const val DEFAULT_ZOOM = 15.0f
        private const val LATNLNG_BOUNDS_PADDING_DP = 25f
    }

    @Inject
    lateinit var locationManager: LocationManager

    @Inject
    lateinit var areaService: AreaService

    @Inject
    lateinit var directionsService: DirectionsService

    private lateinit var mAreaDetailsSheetBehavior: BottomSheetBehavior<View>
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient

    private lateinit var mMap: GoogleMap

    private var isMapReady = false

    private val miniLatLng = LatLng(52.222137, 21.0071188)

    private val mapPolygons = ArrayList<Polygon>()

    private var mLocationPermissionGranted: Boolean = false

    private var mLastKnownLocation: Location? = null

    private var mCameraPosition: CameraPosition? = null

    private var lastSearchedLocation: LatLng? = null

    private var currentRoute: Polyline? = null

    private var currentRouteDestination: Marker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        app.component.inject(this)

        supportActionBar?.setDisplayShowTitleEnabled(false)
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        // Obtain BottomSheetBehavior for manipulating sheet with area details
        mAreaDetailsSheetBehavior = BottomSheetBehavior.from(area_details_sheet)
        mAreaDetailsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        val autocompleteFragment = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment)
                as PlaceAutocompleteFragment

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                lastSearchedLocation = place.latLng
                findRoute(lastSearchedLocation!!)
            }

            override fun onError(status: Status) {
                Log.e(TAG, status.statusMessage.toString())
                Snackbar.make(card_view, status.statusMessage.toString(), Snackbar.LENGTH_LONG)
            }
        })

        // Workaround: https://stackoverflow.com/questions/36130382/android-google-places-api-placeautocompletefragment-clear-button-listener
        autocompleteFragment.view.findViewById<View>(R.id.place_autocomplete_clear_button)
                .setOnClickListener({ view ->
                    autocompleteFragment.setText("")
                    view.visibility = View.GONE

                    currentRoute?.remove()
                    currentRouteDestination?.remove()
                })

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        if (isMapReady) {
            outState!!.putParcelable(KEY_CAMERA_POSITION, mMap.cameraPosition)
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation)
            super.onSaveInstanceState(outState)
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        isMapReady = true

        mMap.setOnPolygonClickListener(this)
        mMap.setOnCameraIdleListener(this)

        getLocationPermission()
        updateLocationUI()
        moveMapToDeviceLocation()
    }

    override fun onCameraIdle() {
        val bounds = mMap.projection.visibleRegion.latLngBounds
        areaService.getAreasInBounds(bounds,
                resultConsumer = {
                    addAreasToMap(it!!)
                }, errorConsumer = { handleError(it) })
    }

    private fun addAreasToMap(newAreas: List<AreaDTO>) {
        // Remove polygons that weren't located in the bounds
        mapPolygons.filter {
            newAreas.none { areaDTO -> (it.tag as AreaDTO).id == areaDTO.id }
        }.forEach {
            Log.d(TAG, "Removing from the map the polygon with id: ${(it.tag as AreaDTO).id}")
            it.remove()
            mapPolygons.remove(it)
        }

        Log.d(TAG, "Polygon count after removing old ones and before adding new ones: ${mapPolygons.size}")

        // Add polygons that weren't located in the bounds
        newAreas.filter {
            mapPolygons.none { polygon -> (polygon.tag as AreaDTO).id == it.id }
        }.forEach {
            Log.d(TAG, "Adding to the map the polygon with id: ${it.id}")

            val polygonOptions = PolygonOptions()
                    .clickable(true)

            // Set polygon color
            when (it.dangerLevel) {
                DangerLevel.LEVEL_1 -> polygonOptions
                        .fillColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel1))
                        .strokeColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel1Dark))
                DangerLevel.LEVEL_2 -> polygonOptions
                        .fillColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel2))
                        .strokeColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel2Dark))
                DangerLevel.LEVEL_3 -> polygonOptions
                        .fillColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel3))
                        .strokeColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel3Dark))
                DangerLevel.LEVEL_4 -> polygonOptions
                        .fillColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel4))
                        .strokeColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel4Dark))
                DangerLevel.LEVEL_5 -> polygonOptions
                        .fillColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel5))
                        .strokeColor(ContextCompat.getColor(applicationContext, R.color.colorDangerLevel5Dark))
            }

            // Set polygon vertices
            it.polygon.points.forEach {
                polygonOptions.add(LatLng(it.latitude, it.longitude))
            }

            val polygon = mMap.addPolygon(polygonOptions)
            polygon.tag = it

            mapPolygons.add(polygon)
        }

        Log.d(TAG, "Polygon count after adding new ones: ${mapPolygons.size}")
    }

    override fun onPolygonClick(polygon: Polygon?) {
        val area = polygon?.tag as AreaDTO

        // Determine polygon bounds to display whole polygon
        val boundsBuilder = LatLngBounds.Builder()
        polygon.points.forEach { boundsBuilder.include(it) }

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(),
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                        LATNLNG_BOUNDS_PADDING_DP, resources.displayMetrics).toInt()))

        area_name_text.text = area.name
        area_description_text.text = area.description
        mAreaDetailsSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun moveMapToDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                val locationResult = mFusedLocationProviderClient.lastLocation

                locationResult.addOnCompleteListener(this, { task ->
                    run {
                        if (task.isSuccessful && task.result != null) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.result
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(mLastKnownLocation?.latitude ?: 52.222137,
                                            mLastKnownLocation?.longitude ?: 21.0071188), DEFAULT_ZOOM))
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.")
                            Log.e(TAG, "Exception: %s", task.exception)
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(miniLatLng, DEFAULT_ZOOM))
                        }
                    }
                })

            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }
    }

    private fun findRoute(destination: LatLng) {
        if (!isMapReady) return

        try {
            if (mLocationPermissionGranted) {
                val locationResult = mFusedLocationProviderClient.lastLocation

                locationResult.addOnCompleteListener { task ->
                    run {
                        if (task.isSuccessful) {
                            mLastKnownLocation = task.result
                            val origin = LatLng(mLastKnownLocation!!.latitude,
                                    mLastKnownLocation!!.longitude)

                            directionsService.findRoute(origin, destination, resultConsumer = {
                                drawRouteOnMap(it!!)
                            }, errorConsumer = { handleError(it) })
                        }
                    }
                }

            }
        } catch (e: SecurityException) {
            Log.e(TAG, e.message)
        }
    }

    private fun drawRouteOnMap(directionsResponse: DirectionsResponseDTO) {
        currentRoute?.remove()
        currentRouteDestination?.remove()

        if (directionsResponse.points.isEmpty()) {
            Snackbar.make(card_view, getString(R.string.no_route_snackbar), Snackbar.LENGTH_LONG).show()
        } else {
            val polylineOptions = PolylineOptions()
                    .color(ContextCompat.getColor(applicationContext, R.color.colorRoute))

            val boundsBuilder = LatLngBounds.Builder()

            var latLng: LatLng? = null

            directionsResponse.points.forEach {
                latLng = LatLng(it.latitude, it.longitude)
                polylineOptions.add(latLng)
                boundsBuilder.include(latLng)
            }

            currentRoute = mMap.addPolyline(polylineOptions)

            val destinationMarkerOptions = MarkerOptions()
                    .position(latLng!!)

            currentRouteDestination = mMap.addMarker(destinationMarkerOptions)

            directionsResponse.points[directionsResponse.points.lastIndex]

            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(),
                    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                            LATNLNG_BOUNDS_PADDING_DP, resources.displayMetrics).toInt()))
        }
    }

    private fun updateLocationUI() {
        if (!isMapReady) {
            return
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings.isMyLocationButtonEnabled = true
                mMap.uiSettings.isCompassEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                mMap.uiSettings.isCompassEnabled = false
                mLastKnownLocation = null
            }
        } catch (e: SecurityException) {
            Log.e(TAG, e.message)
        }

    }

    /* Permission management */

    private fun getLocationPermission() {
        /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        if (ContextCompat.checkSelfPermission(this.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                } else {
                    val alertBuilder = AlertDialog.Builder(this)
                    alertBuilder.setTitle(R.string.location_permission_explanation_title)
                    alertBuilder.setMessage(R.string.location_permission_explanation_description)
                    alertBuilder.setPositiveButton(android.R.string.yes,
                            { _: DialogInterface, _: Int -> })
                    alertBuilder.show()
                }
            }
        }
        updateLocationUI()
    }

    private fun handleError(throwable: Throwable) {
        Log.e(TAG, throwable.message)
        Snackbar.make(card_view, throwable.message.toString(), Snackbar.LENGTH_LONG).show()
    }

}

