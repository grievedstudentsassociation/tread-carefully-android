package pl.gsa.treadcarefully.dto

import pl.gsa.treadcarefully.enums.DangerLevel
import java.util.*

data class AreaDTO(val id: Int? = null,
                   val added: Date? = null,
                   val name: String,
                   val description: String,
                   val dangerLevel: DangerLevel,
                   val polygon: PolygonDTO)