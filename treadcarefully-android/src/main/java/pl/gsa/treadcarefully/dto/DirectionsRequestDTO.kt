package pl.gsa.treadcarefully.dto

data class DirectionsRequestDTO(var origin: LatLngDTO,
                                var destination: LatLngDTO)