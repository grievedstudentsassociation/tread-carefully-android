package pl.gsa.treadcarefully.dto

data class PolygonDTO(var points: List<LatLngDTO>)