package pl.gsa.treadcarefully.dto

data class DirectionsResponseDTO(var points: List<LatLngDTO>)