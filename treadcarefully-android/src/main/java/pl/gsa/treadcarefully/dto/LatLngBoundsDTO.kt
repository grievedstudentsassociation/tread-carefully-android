package pl.gsa.treadcarefully.dto

data class LatLngBoundsDTO(var northEast: LatLngDTO,
                           var southWest: LatLngDTO)