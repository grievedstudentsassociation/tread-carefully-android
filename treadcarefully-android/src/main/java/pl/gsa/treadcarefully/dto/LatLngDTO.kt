package pl.gsa.treadcarefully.dto

data class LatLngDTO(var latitude: Double,
                     var longitude: Double)