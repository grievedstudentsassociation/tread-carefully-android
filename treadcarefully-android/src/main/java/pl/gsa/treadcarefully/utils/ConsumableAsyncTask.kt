package pl.gsa.treadcarefully.utils

import android.os.AsyncTask
import pl.gsa.treadcarefully.ErrorConsumer
import pl.gsa.treadcarefully.ResultConsumer

abstract class ConsumableAsyncTask<Params, Progress, Result>
constructor(private val resultConsumer: ResultConsumer<Result>,
            private val errorConsumer: ErrorConsumer<Throwable>? = null)
    : AsyncTask<Params, Progress, Result?>() {

    private var error: Throwable? = null

    protected abstract fun backgroundTask(vararg params: Params): Result?

    override fun doInBackground(vararg params: Params): Result? {
        try {
            return this.backgroundTask(*params)
        } catch (error: Throwable) {
            this.error = error
        }

        return null
    }

    override fun onPostExecute(result: Result?) {
        if (error != null && errorConsumer != null) {
            errorConsumer.invoke(error!!)
        } else {
            resultConsumer.invoke(result)
        }
    }

}
